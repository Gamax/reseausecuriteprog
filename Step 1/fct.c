#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fct.h"

char* findipDD(char* filename,char* nameToFind){
    FILE *fp ;
    char Buffer[81] ;
    char name[50];
    char* ip = malloc(sizeof(char)*30);
    fp = fopen(filename,"r") ;
    while ( fgets(Buffer,sizeof Buffer,fp),!feof(fp) )
    {

        sscanf(Buffer,"%s %s",ip,name);

        if(strcmp(nameToFind,name) == 0){
            fclose(fp) ;
            return ip;
        }
    }

    free(ip);
    fclose(fp) ;

    return NULL;
}