#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fct.h"


int main(int argc, char** argv){
    if(argc != 3)
        return -1;

    char* result = findipDD(argv[1],argv[2]);

    if(!result){
        printf("Entrée non trouvée !\n");
    }else{
        printf("Entrée trouvée : %s\n", result);
        free(result);
    }

    return 0;



}