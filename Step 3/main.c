#include <stdio.h>
#include <stdlib.h>

#include "../dnslib/dnslib.h"

int main (void)
{
  char  *pchaine =NULL ;
  char  Entree[80] ;
  int Octets[4] ,tmp[4];
 sll_s_TD *p_sll = NULL;
 p_sll = sll_new () ;
  LectureNamedConfTD("named.conf",p_sll) ;
  printf("\n") ;

  printf("------------------------------------------------\n") ;
  printf("Entrez une ip ou un nom de domaine;") ;
  fgets(Entree, sizeof(Entree) , stdin ) ;
  Enleve1013(Entree) ;
  fprintf(stderr,"#%s#\n",Entree) ;
  if (Ipv4ToArray(Entree,tmp)!=-1)
  {
     printf("Recherche Netid %d.%d.%d\n",tmp[0],tmp[1],tmp[2]) ;
     pchaine= RechercheNetIDTD(tmp, p_sll) ;
  }  
  else
  {
      char *Hote ;
      char *Domaine ;
      Hote=SepareHoteDomaine(Entree, &Domaine) ;
      printf("Hote:%s Domaine:%s\n",Hote,Domaine) ;
      pchaine= RechercheNomDomaineTD(Domaine, p_sll ) ;
  }    
  if ( pchaine!=NULL ) 
     printf(" Resultat %s\n",pchaine );
  else
     printf("Rien trouvé !\n") ;
}
