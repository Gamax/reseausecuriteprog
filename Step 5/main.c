#include <stdio.h>
#include <stdlib.h>

#include "../dnslib/dnslib.h"

int main (int argc, char** argv)
{

    if(argc != 2){
        printf("Wrong parameter\n");
        return -1;
    }

    char  *pchaine =NULL ;
  char  Entree[80] ;
  int type;
 sll_s_TD *p_sll = NULL;
 p_sll = sll_new () ;
  LectureDBTD(argv[1],p_sll) ;
  printf("\n") ;
  list_DBTD(p_sll);

  printf("------------------------------------------------\n") ;
  printf("Type (NS:1 A:2 PTR:3 TD:4) : ") ;
  fgets(Entree, sizeof(Entree) , stdin ) ;
  sscanf(Entree,"%d",&type);
  printf("Nom : ");
  fgets(Entree, sizeof(Entree) , stdin ) ;
  Enleve1013(Entree) ;
  fprintf(stderr,"#%s#\n",Entree) ;
  pchaine = Recherche_DBTD(Entree,type,p_sll);
  if ( pchaine!=NULL ) 
     printf(" Resultat %s\n",pchaine );
  else
     printf("Rien trouvé !\n") ;
}
