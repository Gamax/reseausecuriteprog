#include <stdio.h>
#include <string.h>
#include "../udplib/udplib.h"
#include "../dnslib/dnslib.h"
#include <unistd.h>
#include <signal.h>


void die(char *s) {
    perror(s);
    exit(1);
}

static void signal_handler(int sig)
{

}


int main(int argc, char *argv[]) {
    int rc;
    int Desc; //descripteur du socket ouvert
    char buffer[80];
    int tm;
    unsigned int time_interval;
    u_long IpSocket, IpServer;
    u_short PortSocket, PortServer;

    struct DnsMessage message;

    struct sockaddr_in sthis; /* this ce programme */
    struct sockaddr_in sos; /* s = serveur utilisé par senddatagram */
    struct sockaddr_in sor; /* r = remote utilisé par receive datagram */

    struct sigaction act;
    act.sa_handler = signal_handler;
    act.sa_flags = 0 ;
    sigemptyset (&act.sa_mask);
    rc = sigaction (SIGALRM, &act, NULL);

    //init des champs

    memset(&sthis, 0, sizeof(struct sockaddr_in));
    memset(&sos, 0, sizeof(struct sockaddr_in));
    memset(&sor, 0, sizeof(struct sockaddr_in));

    if (argc != 5) {
        printf("cli client portc serveur ports\n");
        exit(1);
    }

    /* Récupération IP & port   */
    IpSocket = inet_addr(argv[1]);
    PortSocket = atoi(argv[2]);

    IpServer = inet_addr(argv[3]);
    PortServer = atoi(argv[4]);

    Desc = creer_socket(SOCK_DGRAM, &IpSocket, PortSocket, &sthis);

    if (Desc == -1)
        die("CreateSockets:");
    else
        fprintf(stderr, "Socket créé : %d\n", Desc);


    sos.sin_family = AF_INET;
    sos.sin_addr.s_addr = IpServer;
    sos.sin_port = htons(PortServer);

    //fin initialisation connexion
    printf("Type (NS:1 A:2 PTR:3 TD:4) : ");
    fgets(buffer, sizeof(buffer) , stdin ) ;
    sscanf(buffer,"%d",&message.type);

    printf("Entrez une ip ou un nom de domaine;") ;
    fgets(buffer, sizeof(buffer) , stdin ) ;
    Enleve1013(buffer) ;
    fprintf(stderr,"#%s#\n",buffer) ;
    strcpy(message.nom,buffer);

    redo:

    rc = SendDatagram(Desc, &message, sizeof(message), &sos);

    if (rc == -1)
        die("SendDatagram:");
    else
        fprintf(stderr, "Envoi de %d bytes\n", rc);

    time_interval = 4 ;
    alarm(time_interval);

    tm = sizeof(message);
    rc = ReceiveDatagram(Desc, &message, tm, &sor);

    if(rc<=0) {
        printf("TIMEOUT !!!\n");
        goto redo;
    }

    alarm(0);

    if (rc == -1)
        die("ReceiveDatagram:");
    else
        fprintf(stderr, "bytes recus:%d:%s\n", rc, message.nom);

    close(Desc);
}
