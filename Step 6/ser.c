/*--------------------------------------
  cphex\ser.c 
  ex01 un seveur recevant des des chaines de caractères
----------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "../udplib/udplib.h"
#include "../dnslib/dnslib.h"

void die(char *s) {
    perror(s);
    exit(1);
}

int main(int argc, char *argv[]) {
    sll_s_TD *p_sll = NULL,*p_sll_zone = NULL;
    p_sll = sll_new();
    p_sll_zone = sll_new();
    LectureNamedConfTD("named.conf", p_sll);
    printf("\n");

    printf("------------------------------------------------\n");


    int rc;
    int Desc;
    int tmp[4];

    struct sockaddr_in sthis; /* this ce programme */
    struct sockaddr_in sos; /* s = serveur */
    struct sockaddr_in sor; /* r = remote */


    u_long IpSocket;
    u_short PortSocket;

    struct DnsMessage message;
    char *buffer;

    int tm;

    memset(&sthis, 0, sizeof(struct sockaddr_in));
    memset(&sos, 0, sizeof(struct sockaddr_in));
    memset(&sor, 0, sizeof(struct sockaddr_in));

    printf("Ceci est le serveur\n");
    if (argc != 3) {
        printf("ser ser port cli\n");
        exit(1);
    }

    /* Récupération IP & port   */
    IpSocket = inet_addr(argv[1]);
    PortSocket = atoi(argv[2]);
    Desc = creer_socket(SOCK_DGRAM, &IpSocket, PortSocket, &sthis);

    if (Desc == -1)
        die("CreateSockets:");

    else
        fprintf(stderr, "CreateSockets %d\n", Desc);


    tm = sizeof(message);
    rc = ReceiveDatagram(Desc, &message, tm, &sor);
    if (rc == -1)
        die("ReceiveDatagram");
    else
        fprintf(stderr, "bytes reçus:%d:%s | Type : %d\n", rc, message.nom, message.type);

    bool isIP = 0;
    char *Hote;
    char *Domaine;

    if (Ipv4ToArray(message.nom, tmp) != -1) {
        printf("Recherche Netid non gérée\n");
        isIP = 1;
    } else {
        Hote = SepareHoteDomaine(message.nom, &Domaine);
        printf("Hote:%s Domaine:%s\n", Hote, Domaine);
        buffer = RechercheNomDomaineTD(Domaine, p_sll);
        printf("Fichier trouvé : %s\n",buffer);
    }

    if(isIP == 1) {
        buffer = "Recherche Netid non gérée\n";
    }else {

        if (buffer == NULL) {
            buffer = "Rien trouvé";

        } else { // si une entrée a été trouvée on ouvre le fichier et on le lis

            LectureDBTD(buffer, p_sll_zone);
            buffer = Recherche_DBTD(Hote, message.type, p_sll_zone);
            if (buffer == NULL)
                buffer = "Rien trouvé dans le fichier zone";
        }
    }

    strcpy(message.nom,buffer);

    /* reponse avec psos */
    rc = SendDatagram(Desc, &message, sizeof(message), &sor);
    if (rc == -1)
        die("SendDatagram");
    else
        fprintf(stderr, "bytes envoyés:%d\n", rc);

    close(Desc);
}
