#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "dnslib.h"
#include <stdbool.h>

typedef struct item
{
    struct item *next;
    void *data;
} item_s;

struct sll
{
    item_s *p_start;
    item_s *list;
};

sll_s_TD *sll_new (void)
{
    sll_s_TD *p_sll = malloc (sizeof *p_sll);
    if (p_sll)
    {
        item_s *p_l = malloc (sizeof *p_l);
        if (p_l)
        {
            p_l->data = NULL;
            p_l->next = NULL;
            p_sll->p_start = p_l;
            p_sll->list = NULL;
        }
        else
        {
            fprintf (stderr, "Memoire insufisante\n");
            exit (EXIT_FAILURE);
        }
    }
    else
    {
        fprintf (stderr, "Memoire insufisante\n");
        exit (EXIT_FAILURE);
    }
    return p_sll;
}
void sll_insert (sll_s_TD * p_sll, void *data)
{
    if (p_sll)
    {
        item_s *p_l = p_sll->list;
        item_s *p_n = NULL;
        p_n = malloc (sizeof (*p_n));
        if (p_n)
        {
            p_n->data = data;
            if (p_l == NULL)
            {
                p_sll->p_start->next = p_n;
                p_n->next = NULL;
            }
            else
            {
                p_n->next = p_l->next;
                p_l->next = p_n;
            }
            p_sll->list = p_n;
        }
        else
        {
            fprintf (stderr, "Memoire insuffisante\n");
            exit (EXIT_FAILURE);
        }
    }
}
void sll_removeNext (sll_s_TD * p_sll)
{
    if (p_sll && p_sll->list)
    {
        item_s *p_l = p_sll->list;
        item_s *p_n = NULL;
        p_n = p_l->next;
        p_l->next = p_n->next;
        free (p_n);
        p_n = NULL;
    }
}
void sll_removeFirst (sll_s_TD * p_sll)
{
    if (p_sll)
    {
        p_sll->list = p_sll->p_start;
        sll_removeNext (p_sll);
    }
}
void sll_next (sll_s_TD * p_sll)
{
    if (p_sll && p_sll->list)
    {
        p_sll->list = p_sll->list->next;
    }
}
void *sll_data (sll_s_TD * p_sll)
{
    return ((p_sll && p_sll->list) ? p_sll->list->data : NULL);
}
void sll_first (sll_s_TD * p_sll)
{
    if (p_sll)
    {
        p_sll->list = p_sll->p_start->next;
    }
}
void sll_last (sll_s_TD * p_sll)
{
    if (p_sll)
    {
        while (p_sll->list->next != NULL)
        {
            sll_next (p_sll);
        }
    }
}
size_t sll_sizeof (sll_s_TD * p_sll)
{
    size_t n = 0;
    if (p_sll)
    {
        sll_first (p_sll);
        while (p_sll->list != NULL)
        {
            n++;
            sll_next (p_sll);
        }
    }
    return n;
}
void sll_delete (sll_s_TD ** pp_sll)
{
    if (pp_sll && *pp_sll)
    {
        sll_first (*pp_sll);
        while ((*pp_sll)->list->next != NULL)
        {
            sll_removeNext (*pp_sll);
        }
        sll_removeFirst (*pp_sll);
        free ((*pp_sll)->list);
        (*pp_sll)->list = NULL;
        free (*pp_sll), *pp_sll = NULL;
    }
}

char* SuffixeDe(char *Suffixe, char* Chaine)
{
    int LongSuffixe,LongChaine ;
// on compare les chaines Ã partir de la fin
    LongSuffixe=strlen(Suffixe) ;
    LongChaine=strlen(Chaine) ;
    if (LongSuffixe == 0 || LongChaine == 0 )
        return NULL ;
    Suffixe= Suffixe + LongSuffixe-1 ;
    Chaine= Chaine + LongChaine-1 ;
    while ( LongChaine && LongSuffixe && *Suffixe==*Chaine )
    {
        LongSuffixe-- ;
        LongChaine-- ;
        Suffixe-- ;
        Chaine-- ;
    }
    if (LongSuffixe)
        return NULL ;
    else
        return (Chaine+1) ;
}

char* SepareHoteDomaine(char *FQDN, char** Domaine)
{
    char *Start  ;
    int TrouvePoint  ;

    Start = FQDN ;
    TrouvePoint=0 ;
    while(*FQDN )
    {
        if ( (*FQDN) == '.' )
        {
            *FQDN = 0 ;
            *Domaine = ++FQDN ;
            TrouvePoint = 1 ;
            break ;
        }
        FQDN++ ;
    }
    if (!TrouvePoint)
        return 0 ;
    else
        return Start ;
}

char* EnleveGL( char *Chaine)
{
    char *ChaineBis, *Copie ;
    ChaineBis = (char *) malloc(strlen(Chaine)+1) ;
    Copie = ChaineBis ;
    while(*Chaine!=0)
    {
        if ( *Chaine=='"' )
            Chaine++ ; /* on ne recopie pas les guillements */
        else
        {
            *Copie++ = *Chaine++ ;
        }
    }
    *Copie=0 ; /* la chaine copiée doit se terminer par un zero */
    return(ChaineBis) ;
}

void Enleve1013(char *Chaine)
{
    while(*Chaine != 0)
    {
        if ( (*Chaine== 10) || (*Chaine==13) )
        {
            *Chaine = 0 ;
            return ;
        }
        Chaine++ ;
    }
}



int 	Ipv4ToArray(char *strsource,int Membre[4])
{
    char   dt[4] ;
    int	NumeroMembre,NombreDigits ;
    int	i,s,lg ;
    char   str[16] ; /* pour ne pas alterer l'original */
    i = 0 ;
    s = 0 ;
    NumeroMembre = 0 ;
    NombreDigits = 0 ;
    memset(Membre,0,sizeof (int)*4) ;
    lg = strlen(strsource) ;
    strncpy(str,strsource,16) ;
    while (i<=lg)  /* Il faut traiter aussi le 0 de fin de chaine */
    {
        if (isdigit(str[i]))
            NombreDigits++ ;
        else
        if (str[i]=='.' || str[i]==0 )
        {
            char tmp[4] ;
            if (NombreDigits==0 || NombreDigits > 3 )
                return -1 ;
            if (NumeroMembre>3)
                return -1 ;
            str[i] = 0 ;
            strcpy(tmp,&str[s]) ;
            Membre[NumeroMembre] = atoi(tmp) ;
            s = i+1 ;
            NumeroMembre++ ;
            NombreDigits = 0 ;
        }
        else /* Pas un chiffre ni un point, ni un z�o */
            return -1 ;
        i++ ;
    }

    return 1 ;
}

bool IsNetidEqual(int IP1[3],struct IPV4 IP2){
    if(IP1[0] == IP2.I1 && IP1[1] == IP2.I2 && IP1[2] == IP2.I3)
        return true;
    return false;
}

void LectureNamedConfTD(char *NomFichier,sll_s_TD *p_l){

FILE* f = fopen(NomFichier,"r");

if(f == NULL){
    printf("Erreur lors de l'ouverture du fichier %s\n",NomFichier);
    return;
}
char buffer[200];
char B1[80],B2[80],B3[80];
struct EntreeNamedConf *item;
int IP[4];
int i;

    while(1)
    {//record par record

        if(fgets(buffer,200,f)==NULL)
            break;

        sscanf(buffer,"%s %s",B1,B2);

        if(strcmp(B1,"zone") !=0){ //si pas une zone on quitte
            printf("Pas une zone\n\n");
        }else {

            item = malloc(sizeof(struct EntreeNamedConf));

            if(item == NULL){
                printf("Erreur allocation mémoire\n");
                exit(0);
            }

            //lecture nom zone
            strcpy(B2, EnleveGL(B2));
            printf("Zone : %s\n", B2);



            if(SuffixeDe(".in-addr.arpa",B2)){ //record DBAddress

                //calcul de la taille
                for(i=0;1;i++){
                    if(B2[i]=='i'){
                        B2[i-1]='\0';
                        break;
                    }
                }

                strncpy(B3,B2,i);
                Ipv4ToArray(B3,IP);
                item->netid.I1 = IP[2];
                item->netid.I2 = IP[1];
                item->netid.I3 = IP[0];
                item->netid.I4 = 0;
                item->Type = DBAddress;
                printf("DbAddress : %d.%d.%d\n",IP[2],IP[1],IP[0]);

            }else{ //record DBdomain

                item->Type = DBDomain;
                strcpy(item->nom,B2);

            }

            //lecture type
            fgets(buffer,200,f);
            sscanf(buffer,"%s %s",B1,B2);
            printf("Type : %s\n", B2);
            if(strcmp(B2,"hint")==0)
                item->Type = Hint;

            //lecture fichier
            fgets(buffer,200,f);
            Enleve1013(buffer) ;
            sscanf(buffer,"%s %s",B1,B2);
            strcpy(B2, EnleveGL(B2));
            printf("Fichier : %s\n", B2);
            strcpy(item->Fichier,B2);


            fgets(buffer,200,f);
            sscanf(buffer,"%s %s %s",B1,B2,B3);

            if(strcmp(B1,"allow-update")==0){ //lecture champs allow-update
                printf("Allow-update : %s\n",B3);
                Ipv4ToArray(B3,IP);
                item->allow_update.I1 = IP[0];
                item->allow_update.I2 = IP[1];
                item->allow_update.I3 = IP[2];
                item->allow_update.I4 = IP[3];

                fgets(buffer,200,f);
                sscanf(buffer,"%s %s",B1,B2);
            }

            //lecture champs perso
            strcpy(B2, EnleveGL(B2));
            printf("Champs perso : %s\n", B2);
            strcpy(item->TD,B2);

            printf("\n------------------------------\n\n");

            sll_insert(p_l,item);

        }

    }

}

char *RechercheNomDomaineTD(char *Domaine, sll_s_TD *p_l) {


    size_t p_lsize = sll_sizeof(p_l);
    sll_first(p_l); //au début
    char* buffer = malloc(sizeof(char)*80);
    struct EntreeNamedConf* item;

    for(int i=0;i<p_lsize;i++){

        item = (struct EntreeNamedConf*) sll_data(p_l);

        if(item->Type== DBDomain && strcmp(Domaine,item->nom)==0 ){
             strcpy(buffer,item->Fichier);
            return buffer;
        }

        sll_next(p_l);
    }


    return NULL;
}

char *RechercheNetIDTD(int *IP, sll_s_TD *p_l) {

    struct EntreeNamedConf* item;
    size_t p_lsize = sll_sizeof(p_l);
    sll_first(p_l);

    for(int i=0;i<p_lsize;i++){

        item = (struct EntreeNamedConf*) sll_data(p_l);

        if(item->Type== DBAddress && IsNetidEqual(IP,item->netid) ){
            return item->Fichier;
        }

        sll_next(p_l);
    }

    return NULL;
}

void LectureDBTD(char *NomFichier,sll_s_TD *p_l) {

    FILE* f = fopen(NomFichier,"r");

    if(f == NULL){
        printf("Erreur lors de l'ouverture du fichier %s\n",NomFichier);
        return;
    }

    char buffer[200];
    char B1[80],B2[80],B3[80],B4[80];
    struct EntreeZone *item;
    int IP[4];
    int i;

    printf("\n---------READING FILE---------\n\n");

    while(1)
    {
        if(fgets(buffer,200,f)==NULL)
            break;

        memset(B1, 0, 80);
        memset(B2, 0, 80);
        memset(B3, 0, 80);
        memset(B4, 0, 80);

        sscanf(buffer,"%s %s %s %s",B1,B2,B3,B4);

        if(strcmp(B2,"IN")!=0 || strcmp(B3,"SOA") ==0) { //on passe le soa on les record SOA et les lignes ne commencant pas par IN
        }else{
            item = malloc(sizeof(struct EntreeZone));

            if(item == NULL){
                printf("Erreur allocation mémoire\n");
                exit(0);
            }

            //détermination type
            if(strcmp(B3,"A")==0)
                item->Type = A;
            if(strcmp(B3,"NS")==0)
                item->Type = NS;
            if(strcmp(B3,"PTR")==0)
                item->Type = PTR;
            if(strcmp(B3,"TD")==0)
                item->Type = TD;

            if(item->Type == TD)
                strcpy(B4,EnleveGL(B4));

            //lecture entrée
            printf("Nom : %s | Type : %s | Valeur : %s\n", B1,B3,B4);

            strcpy(item->Nom,B1);
            strcpy(item->Valeur,B4);

            sll_insert(p_l,item);

            }

        }

    printf("\n------------------------------\n\n");
    }

void list_DBTD(sll_s_TD * p_l) {
    size_t p_lsize = sll_sizeof(p_l);
    sll_first(p_l); //au début
    struct EntreeZone* item;

    printf("\n------------DBFILE------------\n\n");
    printf("Nom | Type | Valeur\n");

    for(int i=0;i<p_lsize;i++){
        item = ((struct EntreeZone *) sll_data(p_l));

        printf("%s | ",item->Nom);

        switch (item->Type){
            case NS:
                printf("NS");
                break;
            case A:
                printf("A");
                break;
            case PTR:
                printf("PTR");
                break;
            case TD:
                printf("TD");
                break;
        }

        printf(" | %s\n",item->Valeur);

        sll_next(p_l);


    }

    printf("\n------------------------------\n\n");
}
char *Recherche_DBTD(char *Nom,int Type,sll_s_TD * p_l) {

    struct EntreeZone* item;
    size_t p_lsize = sll_sizeof(p_l);
    sll_first(p_l);
    char* buffer = malloc(sizeof(char)*80);

    for(int i=0;i<p_lsize;i++){

        item = (struct EntreeZone*) sll_data(p_l);

        if(item->Type == Type && strcmp(item->Nom,Nom)==0 ){
            strcpy(buffer,item->Valeur);
            return buffer;
        }

        sll_next(p_l);
    }

    return NULL;

}


