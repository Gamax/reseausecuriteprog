#ifndef H_LISTESIMPLE
#define H_LISTESIMPLE
#include <stddef.h> /* pour size_t */
#include <stdbool.h>

enum TypeEntree { DBDomain=1, DBAddress=2 ,Hint = 3 } ;
struct IPV4 {
    int I1;
    int I2;
    int I3;
    int I4;
} ;
struct EntreeNamedConf
{
    char nom[80] ; //LE NOM DE DOMAINE
    struct IPV4 netid ; //LEs octets dne netid
    struct IPV4 allow_update; //Vaut 1 si la directive est présente
    char Fichier[80] ; //LE FICHIER .ZONE Associé
    char TD[80] ; //champ perso
    enum TypeEntree Type ; //LE TYPE DBDomain ou DBAddress
};

enum TypeIN { NS=1, A=2 , PTR=3, TD=4 } ;

struct EntreeZone
{
    char Nom[80] ;
    char Valeur[80] ;
    enum TypeIN Type ;
};

struct DnsMessage{
    char nom[80];
    enum TypeEntree type;
};


typedef struct sll sll_s_TD;
sll_s_TD *sll_new (void);
void sll_insert (sll_s_TD *, void *);
void sll_removeNext (sll_s_TD *);
void sll_removeFirst (sll_s_TD *);
void sll_next (sll_s_TD *);
void *sll_data (sll_s_TD *);
void sll_first (sll_s_TD *);
void sll_last (sll_s_TD *);
size_t sll_sizeof (sll_s_TD *);
void sll_delete (sll_s_TD **);

char* SuffixeDe(char *Suffixe, char* Chaine) ;
char* SepareHoteDomaine(char *FQDN, char** Domaine) ;
char* EnleveGL( char *Chaine) ;
void Enleve1013(char *Chaine) ;
int 	Ipv4ToArray(char *strsource,int Membre[4]) ;
bool IsNetidEqual(int IP1[4],struct IPV4 IP2);
void LectureNamedConfTD(char *NomFichier,sll_s_TD *pl);
char* RechercheNetIDTD(int *IP, sll_s_TD *pl);
char* RechercheNomDomaineTD(char *Domaine, sll_s_TD *pl);

void LectureDBTD(char *NomFichier,sll_s_TD *pl) ;
void list_DBTD(sll_s_TD * p_l) ;
char *Recherche_DBTD(char *Nom,int Type,sll_s_TD * p_l) ;





#endif /* not H_LISTESIMPLE */
